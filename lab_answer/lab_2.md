# 1. Apakah perbedaan antara JSON dan XML?
JSON adalah format pertukaran data yang ringan dan tidak bergantung dengan bahasa pemrogramman lainnya. XML dibuat untuk membawa data, bukan menampilkan. XML adalah bahasa markup yang menetapkan aturan-aturan untuk encoding dokumen dalam format yang mudah dibcaca oleh manusia dan mesin. perbedaan keduanya adlaah JSON hanyalah format data sementara XML adalah bahasa markup. JSON dibuat dengan basis javascript dan XML berbasis SGML. jika suatu pryek membutuhkan dokumen markup dan informasi metadata, pilihan yang lebih baik adalah menggunakan XML, sementara untuk pertukaran data yang lebih tersusun JSON menjadi pilihan yang lebih baik.

[Refensi]
>https://www.geeksforgeeks.org/difference-between-json-and-xml/
>https://hackr.io/blog/json-vs-xml
>https://www.javatpoint.com/json-vs-xml
# 2. Apakah perbedaan antara HTML dan XML?
HTML digunakan untuk membuat halaman website dan aplikasi website. sama seperti XML, HTML juga merupakan bahasa markup. HTML digunakan untuk menampilkan data bukan untuk memindahkan data. pada dasarnya. HTML dan XML saling berhubungan. Akan tetapi, HTML digunakan untuk menampilkan data dan menetapkan struktur halaman website, sementara XML menyimpan dan memindahkan data.

[Refensi]
>https://www.upgrad.com/blog/html-vs-xml/
>https://www.geeksforgeeks.org/html-vs-xml/