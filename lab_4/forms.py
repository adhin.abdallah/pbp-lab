from lab_2.models import Note
from django.forms import ModelForm, fields

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields=['to', 'From', 'title', 'message']