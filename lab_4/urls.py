from django.urls import path
from .views import index, add_notes, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_notes),
    path('note-list/', note_list)
]