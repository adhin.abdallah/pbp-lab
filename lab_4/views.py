from django.shortcuts import render
from django.db import models
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def note_list(request):
    notes = Note.objects.all
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)

@login_required(login_url="/admin/login/")
def add_notes(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()  # Save data to DB
            return HttpResponseRedirect('/lab-4')  # Redirect on finish

    # if a GET (or any other method) we'll create a blank form
    # else:
    form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})