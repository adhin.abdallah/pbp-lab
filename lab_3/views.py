from django.shortcuts import render
from django.db import models
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from lab_1.models import Friend
from .forms import FriendForm


# Create your views here.
def index(request):
    friends = Friend.objects.all
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()  # Save data to DB
            return HttpResponseRedirect('/lab-3')  # Redirect on finish

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})
