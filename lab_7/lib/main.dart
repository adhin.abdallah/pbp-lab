// ignore_for_file: avoid_unnecessary_containers, unnecessary_new, prefer_const_constructors

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String? _jk = "";

  List<String> angkatan = ["Quanta", "Maung", "Chronos", "CSUI 2021"];
  String? _angkatan = "Quanta";

  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerPassword = new TextEditingController();
  TextEditingController controllerPesan = new TextEditingController();

  void _pilihJk(String? value) {
    setState(() {
      _jk = value;
    });
  }

  void pilihAngkatan(String? value) {
    setState(() {
      _angkatan = value;
    });
  }

  void kirimData() {
    AlertDialog alertDialog = new AlertDialog(
      content: new SizedBox(
          height: 200.0,
          child: new Column(
            children: [
              new Text("Nama Lengkap : ${controllerNama.text}"),
              new Text("Password : ${controllerPassword.text}"),
              new Text("Pesan : ${controllerPesan.text}"),
              new Text("Jenis Kelamin : $_jk"),
              new Text("Angkatan : $_angkatan"),
              new ElevatedButton(
                  onPressed: () => Navigator.pop(context), child: Text("OK"))
            ],
          )),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.list),
        title: const Text("Formulir"),
        backgroundColor: Colors.teal,
      ),
      body: ListView(
        children: [
          new Container(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                TextField(
                  controller: controllerNama,
                  decoration: InputDecoration(
                      hintText: "Nama Lengkap",
                      labelText: "Nama Lengkap",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                TextField(
                  controller: controllerPassword,
                  obscureText: true,
                  decoration: InputDecoration(
                      hintText: "Password",
                      labelText: "Password",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                TextField(
                  controller: controllerPesan,
                  maxLines: 3,
                  decoration: InputDecoration(
                      hintText: "Pesan",
                      labelText: "Pesan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                RadioListTile(
                  value: "Laki-Laki",
                  title: new Text("Laki-laki"),
                  groupValue: _jk,
                  onChanged: (String? value) {
                    _pilihJk(value);
                  },
                  activeColor: Colors.red,
                ),
                RadioListTile(
                  value: "Perempuan",
                  title: new Text("Perempuan"),
                  groupValue: _jk,
                  onChanged: (String? value) {
                    _pilihJk(value);
                  },
                  activeColor: Colors.red,
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                Row(
                  children: [
                    new Text(
                      "Angkatan ",
                      style: TextStyle(fontSize: 20.0, color: Colors.blue),
                    ),
                    DropdownButton(
                      onChanged: (String? value) {
                        pilihAngkatan(value);
                      },
                      value: _angkatan,
                      items: angkatan.map((String value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                    )
                  ],
                ),
                ElevatedButton(
                  child: Text("OK"),
                  onPressed: () {
                    kirimData();
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
